#![no_std]
#![no_main]
#![allow(unused)]

use panic_halt as _;

use ag_lcd::{Cursor, Display, LcdDisplay, Lines};
use arduino_hal::prelude::*;
use embedded_hal::blocking::delay::DelayUs;
use embedded_hal::digital::v2::OutputPin;
use port_expander::dev::pcf8574::Pcf8574;

const ZERO: i32 = 48;

macro_rules! prompt {
    ( $x:expr ) => {
        ufmt::uwriteln!(&mut $x, "Please enter frequency as an 8-digits value.");
        ufmt::uwriteln!(&mut $x, "Leading zeros required (i.e. 00000001 for 1Hz).");
        ufmt::uwrite!(&mut $x, "> ");
    };
}

#[arduino_hal::entry]
fn main() -> ! {
    let dp = arduino_hal::Peripherals::take().unwrap();
    let pins = arduino_hal::pins!(dp);
    let delay = arduino_hal::Delay::new();
    let mut serial = arduino_hal::default_serial!(dp, pins, 115200);

    // Led
    let mut led = pins.d13.into_output();

    // ad9850 DDS waveform generator chip
    let mut ad9850 = ad9850::Ad9850::new(
        pins.d8.into_output(), // RESET
        pins.d5.into_output(), // DATA
        pins.d3.into_output(), // FQ_UD
        pins.d7.into_output(), // W_CLK
        )
        .into_serial_mode()
        .unwrap();

    // LCD 1602 using I2C interface
    let sda = pins.a4.into_pull_up_input();
    let scl = pins.a5.into_pull_up_input();
    let i2c_bus = arduino_hal::i2c::I2c::new(dp.TWI, sda, scl, 50000);
    let mut i2c_expander = Pcf8574::new(i2c_bus, true, true, true);
    let mut lcd: LcdDisplay<_, _> = LcdDisplay::new_pcf8574(&mut i2c_expander, delay)
        .with_cursor(Cursor::Off)
        .with_lines(Lines::TwoLines)
        .build();
    // Always add end comma to parse as float and stop integer
    let mut buffer = [ZERO as u8; 8];
    let mut digits = 0;

    macro_rules! write_num {
        ($x:expr, $left0s:expr, $decimals:expr) => {
            let mut n = $x;
            let mut printed = false;

            if $x == 0 {
                lcd.write(48);
            }
            for i in (0..$decimals).rev() {
                let base = 10i32.pow(i);
                let v = 48 + n / base;
                if v > 48 {
                    lcd.write((48 + n / base) as u8);
                    n -= n / base * base;
                    printed = true;
                } else if ($left0s && n > 0) || (!$left0s && printed) {
                    lcd.write(48);
                }
            }
        };
    }

    macro_rules! set_frequency {
        ( $x:expr ) => {
            let (exp, mant, unit) = if $x >= 1_000_000 {
                ($x / 1_000_000, $x % 1_000_000, "MHz")
            } else if $x >= 1_000 {
                ($x / 1_000, $x % 1_000, "KHz")
            } else {
                ($x, 0, "Hz")
            };

            // Console print
            ufmt::uwriteln!(
                &mut serial,
                "Setting frequency to {}.{:06x} {}",
                exp,
                mant,
                unit
                );

            // LCD print
            lcd.clear();
            lcd.set_position(0, 0);
            lcd.print("Freq: ");
            write_num!(exp, false, 3);
            lcd.print(".");
            write_num!(mant, true, match unit { "MHz" => 6, _ => 3 });
            lcd.print(unit);
            lcd.set_position(0, 1);
            lcd.print("Duty: 50%");

            ad9850.set_frequency($x as f32);
            prompt!(serial);
        };
    }

    // Set frequency
    set_frequency!(2_500_000);

    // Loop forever blocking for data at serial
    loop {
        let b = nb::block!(serial.read()).void_unwrap();
        if b == 10 || b == 13 {
            serial.write(10);
            let freq: i32 = buffer
                .into_iter()
                .rev()
                .enumerate()
                .map(|(i, x)| (x as i32 - ZERO) * 10u32.pow(i as u32) as i32)
                .sum();
            if freq < 1 || freq > 62_500_000 {
                ufmt::uwriteln!(&mut serial, "Bad frequency {} < 1Hz or > 62.5Mhz", freq);
                prompt!(serial);
            } else {
                set_frequency!(freq);
            }
            digits = 0;
            buffer = [ZERO as u8; 8];
        } else if b > 47 && b < 58 {
            if digits < 8 {
                serial.write(b);
                buffer[digits] = b;
                digits += 1;
            }
        }
    }
}

fn test_macros() {
    /*
       println!("MHz");
       set_frequency!(12_345_678);
       set_frequency!(60_000_001);
       set_frequency!(10_000_001);
       set_frequency!(2_500_000);
       set_frequency!(1_000_001);
       set_frequency!(1_000_000);
       println!("KHz");
       set_frequency!(123_456);
       set_frequency!(100_000);
       set_frequency!(101_000);
       set_frequency!(100_001);
       set_frequency!(11_000);
       set_frequency!(10_001);
       set_frequency!(10_000);
       set_frequency!(1_001);
       set_frequency!(1_000);
       println!("HZ");
       set_frequency!(123);
       set_frequency!(101);
       set_frequency!(100);
       set_frequency!(11);
       set_frequency!(10);
       set_frequency!(1);
       */
}
